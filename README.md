## Introduction

A plugin for Bluetooth functionality on Android and IOS.<br>This plugin uses [flutter_reactive_ble](https://pub.dev/packages/flutter_reactive_ble) for interoperating with the native platforms.

---

## Setup

### Android

Add the following permissions to the **AndroidManifest.xml**, located at **android/app/src/main/AndroidManifest.xml**.
Put the permissions in the **manifest** tag, infront of the **application** tag.

```xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="com.example.project">

    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <uses-permission android:name="android.permission.BLUETOOTH" />
    <uses-permission android:name="android.permission.BLUETOOTH_ADMIN" />
 <application>
 ...
```

### IOS

In the **ios/Runner/Info.plist** you have to add:

```xml
<dict>
  <key>NSBluetoothAlwaysUsageDescription</key>  
  <string>Need BLE permission</string>  
  <key>NSBluetoothPeripheralUsageDescription</key>  
  <string>Need BLE permission</string>  
  <key>NSLocationAlwaysAndWhenInUseUsageDescription</key>  
  <string>Need Location permission</string>  
  <key>NSLocationAlwaysUsageDescription</key>  
  <string>Need Location permission</string>  
  <key>NSLocationWhenInUseUsageDescription</key>  
  <string>Need Location permission</string>
...
```

---

## Errors

### Error on scanning

If an error similar to [this github issue](https://github.com/pauldemarco/flutter_blue/issues/662#issuecomment-730831191) arises, then use [this comment](https://github.com/pauldemarco/flutter_blue/issues/662#issuecomment-730831191) as a fix.<br>
Essentially you only have to set the following flags in the `release` scope of the `buildTypes` scope.

This is your current `release` build type in **android/app/build.gradle**.
```groovy
buildTypes {
  release {  
    // TODO: Add your own signing config for the release build.
    // Signing with the debug keys for now, so `flutter run --release` works.
    signingConfig signingConfigs.debug
  }
}
```

Change the `release` build type in **android/app/build.gradle** to this.
```groovy
buildTypes {
  release {  
    // TODO: Add your own signing config for the release build.
    // Signing with the debug keys for now, so `flutter run --release` works.
    signingConfig signingConfigs.debug
    
    // Fix for https://github.com/pauldemarco/flutter_blue/issues/662#issuecomment-730831191
    shrinkResources false
    minifyEnabled false
  }
}
```

### App crashing in release on Android

In case you are using ProGuard add the following snippet to your **android/app/proguard-rules.pro** file:

```
-keep class com.signify.hue.** { *; }
```

This will solve this [issue](https://github.com/PhilipsHue/flutter_reactive_ble/issues/131).