part of "../../bluetooth.dart";

/// The [BluetoothStateService] is responsible for enabling and disabling
/// the bluetooth functionality of the device this app is run on.
class BluetoothStateService implements _Disposable {
  BluetoothStateService._() {
    _stateNotifier = AdvancedValueNotifier(BluetoothState.unknown);
    // TODO(obemu): Add support for _stateServiceEventChannel on IOS.
    // _stateChangedSub =
    //     _stateServiceEventChannel
    //         .receiveBroadcastStream()
    //         .map((event) => _getBluetoothStatefromNativeState(event as int?))
    //         .listen((event) => _stateNotifier.value = event);

    _stateChangedSub = FlutterReactiveBle()
        .statusStream
        .map(_getBluetoothStatefromBleStatus)
        .listen((event) => _stateNotifier.value = event);
  }

  static const _eventChannelId = ".state_service";

  late final AdvancedValueNotifier<BluetoothState> _stateNotifier;
  late final StreamSubscription<BluetoothState> _stateChangedSub;

  bool get isEnabled => BluetoothState.on == state.value;

  bool get isDisabled => !isEnabled;

  AdvancedValueListenable<BluetoothState> get state => _stateNotifier;

  /// Get the current state of the native BluetoothStateService and
  /// update the value of [state] accordingly.
  // Keep this method as an async method, because when there finally is
  // a native implementation for "getState" on IOS we have to await the
  // returned future from the method channel.
  Future<BluetoothState> getState() async {
    // TODO(obemu): Add support for getState on IOS.
    // "getState" always returns null on IOS
    // final result = await _methodChannel.invokeMethod<int>("getState");
    // final state = _getBluetoothStatefromNativeState(result);
    // _stateNotifier.value = state;
    // return state;

    final state = _getBluetoothStatefromBleStatus(FlutterReactiveBle().status);
    _stateNotifier.value = state;
    return state;
  }

  /// `True` to indicate bluetooth is enabled.
  /// `False` if an error occured.
  ///
  /// Currently this method only works on Android.
  Future<bool> enable() async {
    if (isEnabled) return true;

    final stateCompleter = Completer<bool>();

    void stateListener(BluetoothState event) {
      if (stateCompleter.isCompleted) return;

      // This event is only emitted on android devices, if the location
      // services (GPS) are disabled by the user.
      //
      // It does not really matter if the location servies are enabled or
      // disabled, because they are not necessary for scan operations.
      // If the location services were enabled, the event right after
      // BluetoothState.locationServicesDisabled would be BluetoothState.ready,
      // so we can interpret BluetoothState.locationServicesDisabled as
      // BluetoothState.ready.
      //
      // See:
      // https://github.com/dariuszseweryn/RxAndroidBle/blob/master/rxandroidble/src/main/java/com/polidea/rxandroidble2/RxBleClient.java
      if (BluetoothState.locationServicesDisabled == event) {
        stateCompleter.complete(true);
        return;
      }

      if (BluetoothState.on == event) {
        stateCompleter.complete(true);
        return;
      }

      if (BluetoothState.unknown == event ||
          BluetoothState.unsupported == event ||
          BluetoothState.unauthorized == event) {
        stateCompleter.complete(false);
        return;
      }
    }

    state.addValueListener(stateListener);

    // TODO(obemu): Add support for enable on IOS.
    // "enable" always returns null on IOS
    if (!(await _methodChannel.invokeMethod<bool>("enable") ?? false)) {
      state.removeValueListener(stateListener);
      return false;
    }

    if (!await stateCompleter.future) {
      state.removeValueListener(stateListener);
      return false;
    }

    state.removeValueListener(stateListener);

    return true;
  }

  /// `True` to indicate bluetooth is disabled.
  /// `False` if an error occured.
  ///
  /// Currently this method only works on Android.
  Future<bool> disable() async {
    if (isDisabled) return true;

    final completer = Completer<bool>();
    void stateListener(BluetoothState event) {
      final event = state.value;

      if (completer.isCompleted) return;

      if (BluetoothState.off == event) completer.complete(true);

      if (BluetoothState.unknown == event ||
          BluetoothState.unsupported == event ||
          BluetoothState.unauthorized == event) completer.complete(false);
    }

    state.addValueListener(stateListener);

    // TODO(obemu): Add support for disable on IOS.
    // "disable" always returns null on IOS
    if (!(await _methodChannel.invokeMethod<bool>("disable") ?? false)) {
      state.removeValueListener(stateListener);
      return false;
    }

    if (!await completer.future) {
      state.removeValueListener(stateListener);
      return false;
    }

    state.removeValueListener(stateListener);

    return true;
  }

  @override
  @protected
  Future<void> dispose() async {
    await _stateChangedSub.cancel();
    await _stateNotifier.dispose();
  }
}
