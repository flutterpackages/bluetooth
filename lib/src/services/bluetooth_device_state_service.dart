part of "../../bluetooth.dart";

class BluetoothDeviceStateService implements _Disposable {
  BluetoothDeviceStateService._() : _deviceConnections = {};

  final Map<MutableRef<_BluetoothDeviceWrapper>,
      StreamSubscription<ConnectionStateUpdate>?> _deviceConnections;

  /// ## Description
  ///
  /// A stream providing connection updates for all the connected
  /// [BluetoothDevice]s.
  ///
  ///
  /// ## Issues
  ///
  /// ### Android
  ///
  /// Once the connected device is randomly turned off, or out of range it
  /// takes about `20 seconds` for the diconnect event to be emitted this
  /// is because of a hardcoded value in the [android source code](https://miro.medium.com/max/875/1*Wygkykba-vqWgn2r8wRoqA.jpeg).
  ///
  /// This [article](https://blog.classycode.com/a-short-story-about-android-ble-connection-timeouts-and-gatt-internal-errors-fa89e3f6a456)
  /// explains the issue in greater detail.
  ///
  /// * [Issue 37119344](https://issuetracker.google.com/issues/37119344)
  /// * [Issue 37074926](https://issuetracker.google.com/issues/37074926)
  Stream<ConnectionUpdate> get connectionUpdates =>
      FlutterReactiveBle().connectedDeviceStream.map((event) {
        final deviceRef = _deviceConnections.keys.firstWhere(
          (element) => element.value.device.id == event.deviceId,
        );
        final update = ConnectionUpdate._(
          device: deviceRef.value.device,
          connectionState: event.connectionState,
          failure: event.failure,
        );

        // We have to update the connection state in this stream
        // because otherwise if someone calls isConnected/isDisconnected
        // in a listener for connectionUpdates then the real
        // connection state present in deviceRef.value.connectionStateNotifier
        // has not been updated yet.
        deviceRef.value.connectionStateUpdate = event;

        return update;
      });

  /// Get a [Map] containing all [BluetoothDevice], that you
  /// passed to any function of `this` [BluetoothDeviceStateService],
  /// associated with their current connection state.
  Map<BluetoothDevice, DeviceConnectionState> get states =>
      _deviceConnections.map((key, value) => MapEntry(
            key.value.device,
            key.value.connectionStateUpdateListenable.isNotEmpty
                ? key
                    .value.connectionStateUpdateListenable.value.connectionState
                // If the connectionStateUpdateListenable is still empty then
                // this BluetoothDevice can still not be connected.
                : DeviceConnectionState.disconnected,
          ));

  /// Get the [MutableRef] key from [_deviceConnections], where [device] equals
  /// [_BluetoothDeviceWrapper.device] of the value of the [MutableRef] key.
  ///
  /// If there is none, then a new entry with [device] as a key and `null`
  /// as a value is added and returned.
  MutableRef<_BluetoothDeviceWrapper> _getDeviceRefKey(
    BluetoothDevice device,
  ) {
    final key = _BluetoothDeviceWrapper(device);

    // Return the MapEntry, where the Ref of _DiscoveredDeviceWrapper
    // has a value that equals key.
    for (final item in _deviceConnections.entries) {
      if (item.key.value == key) return item.key;
    }

    // If a Ref of key was not already present in _deviceConnections,
    // add a new one.
    final entry = MapEntry<MutableRef<_BluetoothDeviceWrapper>,
            StreamSubscription<ConnectionStateUpdate>?>(
        key.ref<_BluetoothDeviceWrapper>(), null);

    _deviceConnections[entry.key] = entry.value;

    return entry.key;
  }

  /// Get the [EmptyValueListenable] for [ConnectionStateUpdate]s of [device].
  EmptyValueListenable<ConnectionStateUpdate> getConnectionStateListenable(
          BluetoothDevice device) =>
      _getDeviceRefKey(device).value.connectionStateUpdateListenable;

  bool isConnected(BluetoothDevice device) {
    // Call _getDeviceRefKey to add device as a key to _deviceConnections,
    // if it was not already present.
    _getDeviceRefKey(device);

    return DeviceConnectionState.connected == states[device];
  }

  bool isDisconnected(BluetoothDevice device) {
    // Call _getDeviceRefKey to add device as a key to _deviceConnections,
    // if it was not already present.
    _getDeviceRefKey(device);

    return DeviceConnectionState.disconnected == states[device];
  }

  /// ## Description
  ///
  /// * Scans for [device] and connects to it in case it is found and
  ///   it is advertising the services specified in [services].
  ///
  /// ---
  /// * If [prescanDuration] and [services] are non-null, then there
  ///   will be an explicit scan for a [BluetoothDevice] that contains
  ///   [services] and an id that matches the id of [device].
  ///
  ///
  /// ## Parameters
  ///
  /// * **[device]**:
  ///   The [BluetoothDevice] you want to connect to.
  ///
  /// ---
  /// * **[connectionTimeout]**:
  ///   If non-null and a connection is not established before
  ///   [connectionTimeout] expires, then the pending connection attempt will be
  ///   cancelled. On Android when no timeout is specified the `autoConnect`
  ///   flag is set in the [connectGatt()](https://developer.android.com/reference/android/bluetooth/BluetoothDevice#connectGatt(android.content.Context,%20boolean,%20android.bluetooth.BluetoothGattCallback))
  ///   call, otherwise it is cleared.
  ///
  /// ---
  /// * **[prescanDuration]**:
  ///   If non-null, the amount of time BLE disovery should run in order
  ///   to find [device].
  ///
  /// ---
  /// * **[services]**:
  ///   If non-null, then only look for [BluetoothDevice]s that have services
  ///   from this list.
  ///
  /// ---
  /// * **[servicesWithCharacteristicsToDiscover]**:
  ///   Scan only for the specific services and characteristics mentioned in
  ///   this map, this can improve the connection speed on iOS since no full
  ///   service discovery will be executed.
  ///
  ///
  /// ## Return
  ///
  /// * `True` if successfully connected to [device]
  ///
  /// * `False` if no connection could be established,
  ///   or [connectionTimeout] expired.
  Future<bool> connect({
    required BluetoothDevice device,
    Duration? connectionTimeout,
    Duration? prescanDuration,
    List<Uuid>? services,
    Map<Uuid, List<Uuid>>? servicesWithCharacteristicsToDiscover,
  }) async {
    assert(null == connectionTimeout || !connectionTimeout.isNegative);
    assert(
        (null == prescanDuration && null == services) ||
            (null != prescanDuration && null != services),
        "Either prescanDuration and services are null, "
        "or both of them are non-null. "
        "You cannot set only one of them to null.");

    _log(message: "Connecting to $device.");

    if (isConnected(device)) {
      _log(message: "$device is already connected.");
      return true;
    }

    final deviceRef = _getDeviceRefKey(device);
    final completer = Completer<ConnectionStateUpdate>();

    final connectionStream = (null == prescanDuration || null == services)
        ? FlutterReactiveBle().connectToDevice(
            id: deviceRef.value.device.id,
            connectionTimeout: connectionTimeout,
            servicesWithCharacteristicsToDiscover:
                servicesWithCharacteristicsToDiscover,
          )
        : FlutterReactiveBle().connectToAdvertisingDevice(
            id: deviceRef.value.device.id,
            withServices: services,
            connectionTimeout: connectionTimeout,
            prescanDuration: prescanDuration,
            servicesWithCharacteristicsToDiscover:
                servicesWithCharacteristicsToDiscover,
          );

    final sub = connectionStream.listen((event) {
      _log(message: "connectToDevice state update listener:\n$event");

      if (completer.isCompleted) return;

      if (DeviceConnectionState.connected == event.connectionState ||
          DeviceConnectionState.disconnected == event.connectionState) {
        completer.complete(event);
      }
    });

    final result = await completer.future;

    // If the device is disconnected, an error occured.
    if (DeviceConnectionState.disconnected == result.connectionState) {
      _log(
        message: "Device is not connected.",
        error: "Error while connecting to $device.",
        level: Level.error,
      );

      // device is disconnected
      deviceRef.value.connectionStateUpdate = result;
      await sub.cancel();
      _deviceConnections[deviceRef] = null;

      return false;
    }

    // device is connected
    _deviceConnections[deviceRef] = sub;
    deviceRef.value.connectionStateUpdate = result;
    sub.onData((e) => deviceRef.value.connectionStateUpdate = e);

    _log(
      message: "Successfully connected to device:\n$device"
          "\n\nconnections:\n$_deviceConnections",
    );

    return true;
  }

  /// The returned [Future] completes, when the [device] has been disconnected.
  Future<void> disconnect({
    required BluetoothDevice device,
  }) async {
    _log(message: "Disconnecting from $device.");

    final deviceRef = _getDeviceRefKey(device);

    // If the value for device equals null, then the _device is not connected.
    if (null == _deviceConnections[deviceRef]) {
      _log(message: "$device is already disconnected.");
      return;
    }

    // Canceling the StreamSubscription disconnects the device.
    //
    // Also the returned Future from cancel() is from the underlying
    // disconnect method of the flutter_reactive_ble library.
    await _deviceConnections[deviceRef]!.cancel();

    _deviceConnections[deviceRef] = null;

    deviceRef.value.connectionStateUpdate = deviceRef
        .value.connectionStateUpdateListenable.value
        .copyWith(connectionState: DeviceConnectionState.disconnected);

    _log(
      message: "Successfully disconnected from device:\n$device"
          "\n\nconnections:\n$_deviceConnections",
    );
  }

  @override
  @protected
  Future<void> dispose() async {
    for (final item in _deviceConnections.entries) {
      await item.key.value.dispose();
      await item.value?.cancel();
    }

    _deviceConnections.clear();
  }
}

class _BluetoothDeviceWrapper implements _Disposable {
  _BluetoothDeviceWrapper(this._device)
      : _connectionStateNotifier = EmptyValueNotifier.empty() {
    _connectionStateNotifier.addValueListener((value) {
      // If the [device] was disconnected, set its rssi to 0.
      if (DeviceConnectionState.disconnected == value.connectionState)
        _device = _device.copyWith(rssi: 0);

      _log(
          message: "Listener connectionStateNotifier of\n$device"
              "\n\nconnection update:\n$value");
    });
  }

  BluetoothDevice _device;
  final EmptyValueNotifier<ConnectionStateUpdate> _connectionStateNotifier;

  @override
  bool operator ==(Object other) {
    if (other is! _BluetoothDeviceWrapper) return false;
    if (other.device != device) return false;
    return true;
  }

  BluetoothDevice get device => _device;

  // Adding 37 * 17 to device.hashCode because the
  // hashCode of _BluetoothDeviceWrapper should not be exactly
  // the same as the hashCode of BluetoothDevice.
  @override
  int get hashCode => 37 * 17 + device.hashCode;

  EmptyValueListenable<ConnectionStateUpdate>
      get connectionStateUpdateListenable => _connectionStateNotifier;

  /// Set the connection state of [device].
  set connectionStateUpdate(ConnectionStateUpdate update) {
    // Only set if connectionStateNotifier.value is different from update
    if (_connectionStateNotifier.isEmpty ||
        _connectionStateNotifier.value != update) {
      _connectionStateNotifier.value = update;
    }
  }

  @override
  String toString() => "${describeIdentity(this)}"
      "("
      "device: $device, "
      "_connectionStateNotifier: $_connectionStateNotifier"
      ")";

  @override
  @protected
  Future<void> dispose() async {
    await _connectionStateNotifier.dispose();
  }
}
