part of "../../bluetooth.dart";

class BluetoothCommunicationService {
  const BluetoothCommunicationService._(this._deviceStateServiceRef);

  /// Even though the type of this property is a [ConstRef] of a nullable
  /// [BluetoothDeviceStateService], the [BluetoothDeviceStateService] is
  /// never null.
  ///
  /// So it is always safe to promote the value of the [ConstRef] from
  /// [BluetoothDeviceStateService?] to [BluetoothDeviceStateService].
  final ConstRef<BluetoothDeviceStateService?> _deviceStateServiceRef;

  /// Returns `null`, if [device] is not connected.
  Future<List<DiscoveredService>?> discoverServices(
    BluetoothDevice device, {
    OnExceptionCallback? onException,
  }) async {
    assert(
      _deviceStateServiceRef.value!.isConnected(device),
      "You should not call BluetoothCommunicationService.discoverServices, "
      "if the supplied device is not connected.",
    );

    List<DiscoveredService>? services;

    try {
      services = await FlutterReactiveBle().discoverServices(device.id);
    } on Exception catch (e) {
      _log(error: e, level: Level.error);
      onException?.call(e);
    }

    return services;
  }

  /// Read the value of a [QualifiedCharacteristic].
  ///
  /// Returns `null` if an error occured.
  Future<List<int>?> read(
    QualifiedCharacteristic characteristic, {
    OnFutureErrorCallback<void, Object?>? onError,
  }) async {
    List<int>? data;

    await FlutterReactiveBle()
        .readCharacteristic(characteristic)
        .then((value) => data = value)
        .onError((error, stackTrace) {
      _log(
        message: "Error while trying to read $characteristic",
        level: Level.error,
        error: error,
        stackTrace: stackTrace,
      );
      onError?.call(error, stackTrace);
      return [];
    });

    return data;
  }

  /// Writes a value to the specified [characteristic].
  ///
  /// If [withResponse] is `true` then an acknowledgement is awaited.
  ///
  /// If an error occured during the write process, then `false`
  /// gets returned and [onError], if `non-null`, gets called.
  Future<bool> write({
    required QualifiedCharacteristic characteristic,
    required List<int> value,
    bool withResponse = false,
    OnFutureErrorCallback<void, Object?>? onError,
  }) async {
    _log(
      level: Level.verbose,
      message: "write:\n<${value.length}>$value",
    );

    var errorOccured = false;

    void errorHandler(error, stackTrace) {
      _log(
        message: "Error while trying to read $characteristic",
        level: Level.error,
        error: error,
        stackTrace: stackTrace,
      );
      onError?.call(error, stackTrace);
      errorOccured = true;
    }

    if (withResponse) {
      await FlutterReactiveBle()
          .writeCharacteristicWithResponse(characteristic, value: value)
          .onError(errorHandler);
    } else {
      await FlutterReactiveBle()
          .writeCharacteristicWithoutResponse(characteristic, value: value)
          .onError(errorHandler);
    }

    return !errorOccured;
  }

  /// Subscripe to [characteristic] to receive continuous notifications
  /// when the [QualifiedCharacteristic]s value changes.
  ///
  /// Only works with [QualifiedCharacteristic] that can send notifications.
  ///
  /// The subscription ends when the returned [StreamSubscription] is
  /// cancelled, or the connection to the [BluetoothDevice] is cut.
  StreamSubscription<List<int>> subscribe({
    required QualifiedCharacteristic characteristic,
    void Function(List<int> event)? onData,
    Function? onError,
    VoidCallback? onDone,
    bool? cancelOnError,
  }) =>
      FlutterReactiveBle().subscribeToCharacteristic(characteristic).listen(
            onData,
            onError: onError,
            onDone: onDone,
            cancelOnError: cancelOnError,
          );

  /// Request a specific MTU for a connected device.
  ///
  /// Returns the actual MTU negotiated.
  ///
  /// For reference:
  ///
  /// * BLE 4.0–4.1 max ATT MTU is 23 bytes
  /// * BLE 4.2–5.1 max ATT MTU is 247 bytes
  Future<int> requestMtu({
    required BluetoothDevice device,
    required int desiredMtu,
  }) async {
    assert(desiredMtu >= 23);

    return FlutterReactiveBle().requestMtu(
      deviceId: device.id,
      mtu: desiredMtu,
    );
  }
}
