part of "../../bluetooth.dart";

class BluetoothScannerService implements _Disposable {
  BluetoothScannerService._()
      : _isScanningNotifier = AdvancedValueNotifier(false);

  final AdvancedValueNotifier<bool> _isScanningNotifier;
  _ScanCompleter? _completer;

  AdvancedValueListenable<bool> get isScanning => _isScanningNotifier;

  /// ## Description
  ///
  /// Scan for nearby bluetooth devices.
  ///
  // See DiscoveredDevicesRegistry in flutter_reactive_ble-5.0.1/lib/src/discovered_devices_registry.dart
  /// Once a device has been discovered, its' id gets added to an internal
  /// [List], therefore [onFoundDevice] is only called once for every
  /// discovered device. [onFoundDevice] is only called again if a
  /// [BluetoothDevice] with an id, that differs from the previously discovered
  /// ones is found. Due to the id of each [BluetoothDevice] being an [Uuid],
  /// it is very unlikely to find a batch of devices in the same area with
  /// the same [Uuid].
  ///
  /// Throws a [ScanInProgressException], if [isScanning] is `true`, when
  /// calling this method.
  ///
  ///
  /// ## Parameters
  ///
  /// * **[onFoundDevice]**:
  ///   [onFoundDevice] is called every time a [BluetoothDevice] has been found.
  ///   Returning `true` from [onFoundDevice], indicates that you found the
  ///   [BluetoothDevice] you where looking for and therefore want to stop
  ///   the scan process.
  ///
  /// ---
  /// * **[timeout]**:
  ///   If [timeout] is `non-null`, then the scan process gets stopped
  ///   after [timeout] has run out, even though [onFoundDevice] may
  ///   not have returned true yet.
  ///
  /// ---
  /// * **[serviceFilter]**:
  ///   If [serviceFilter] is `non-null`, only [BluetoothDevice]s with a
  ///   [DiscoveredService] that has a [Uuid] from [serviceFilter]
  ///   will be passed to [onFoundDevice] and returned from [scan].
  ///
  /// ---
  /// * **[requireAndroidLocationServicesEnabled]**:
  ///   Specifies whether to check if location services are enabled before
  ///   scanning. When set to true and location services are disabled, the
  ///   scan operation is aborted and there will be logs of an [Exception]
  ///   being thrown and the returned [List] of [BluetoothDevice]s is empty.
  ///   Default is `true`. Setting the value to `false` can result in not
  ///   finding BLE peripherals on some Android devices.
  ///
  ///
  /// ## Returns
  ///
  /// Always returns all [BluetoothDevice]s that where found until the
  /// scan process was stopped.
  Future<List<BluetoothDevice>> scan({
    required OnFoundDeviceCallback onFoundDevice,
    Duration? timeout,
    List<Uuid>? serviceFilter,
    bool requireAndroidLocationServicesEnabled = true,
  }) async {
    assert(
      null == timeout || (timeout >= const Duration(seconds: 5)),
      "If timeout is non-null, it has to be "
      "greater than, or equal to 5 seconds.",
    );

    if (isScanning.value) return Future.error(const ScanInProgressException());

    final List<BluetoothDevice> devices = [];

    _isScanningNotifier.value = true;

    _completer = _ScanCompleter(FlutterReactiveBle()
        // By anaylizying the Kotlin source file
        // ScanDevicesHandler.kt in the plugin reactive_ble_mobile
        // you can see that the native stopDeviceScan method
        // is called if the StreamSubscription is cancelled.
        .scanForDevices(
          scanMode: ScanMode.balanced,
          withServices: serviceFilter ?? [],
          requireLocationServicesEnabled: requireAndroidLocationServicesEnabled,
        )
        // If requireAndroidLocationServicesEnabled is true, but the locaton
        // services are not enabled, an exception is thrown and the scan
        // operation does not start.
        // ignore: avoid_types_on_closure_parameters
        .handleError((Object error, StackTrace st) {
          _log(
            error: error,
            level: Level.error,
            stackTrace: st,
            message: "Completing scan.",
          );

          _completer?.complete();
        })
        .map((event) => BluetoothDevice._(event))
        .listen((e) {
          devices.add(e);

          // If the device was found and timeout was not set,
          // complete the scan process.
          if (onFoundDevice(e)) {
            _completer?.complete();
            _log(
              level: Level.info,
              message: "Scan completed, device found!\nDevice: $e.",
            );
          }
        }));

    // If timeout was set, complete the scan process after timeout,
    // even if onFoundDevice has not returned true yet.
    Timer? timer;
    if (null != timeout) {
      timer = Timer(timeout, () {
        _completer?.complete();
        _log(
          level: Level.info,
          message: "Scan completed after timeout ($timeout).",
        );
      });
    }

    _log(message: "Waiting for scan to complete.");

    await _completer!.future;
    timer?.cancel();
    _completer = null;

    _isScanningNotifier.value = false;

    _log(message: "Scan completed with the following devices:\n\n$devices");

    return devices;
  }

  /// Stop the currently active scan process, if there is one.
  Future<void> stopScan() async {
    _completer?.complete();
    return _completer?.future;
  }

  @override
  @protected
  Future<void> dispose() async {
    await _isScanningNotifier.dispose();
    await _completer?.dispose();
    _completer = null;
  }
}

class _ScanCompleter implements _Disposable {
  /// The [_scanSub] gets cancelled, once [complete], or [dispose]
  /// are called.
  _ScanCompleter(this._scanSub) {
    _completer = Completer()..future.whenComplete(_scanSub.cancel);
  }

  final StreamSubscription<BluetoothDevice> _scanSub;
  late final Completer<void> _completer;

  bool get isCompleted => _completer.isCompleted;

  Future<void> get future => _completer.future;

  void complete() {
    if (isCompleted) return;
    _completer.complete();
  }

  @override
  Future<void> dispose() async {
    complete();
  }
}
