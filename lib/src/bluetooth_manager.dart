// ignore_for_file: avoid_classes_with_only_static_members

part of "../bluetooth.dart";

abstract class BluetoothManager {
  static Level _nativeLogLevel = Level.verbose;
  static OnLogCallback? _onLogCallback;

  static final MutableRef<BluetoothStateService?> _stateServiceRef =
      MutableRef(null);

  static final MutableRef<BluetoothScannerService?> _scannerServiceRef =
      MutableRef(null);

  static final MutableRef<BluetoothDeviceStateService?> _deviceStateServiceRef =
      MutableRef(null);

  static final MutableRef<BluetoothCommunicationService?>
      _communicationServiceRef = MutableRef(null);

  static BluetoothStateService get state {
    assert(
        null != _stateServiceRef.value,
        "Call BluetoothManager.initialize before "
        "accessing any static members.");

    return _stateServiceRef.value!;
  }

  static BluetoothScannerService get scanner {
    assert(
        null != _scannerServiceRef.value,
        "Call BluetoothManager.initialize before "
        "accessing any static members.");

    return _scannerServiceRef.value!;
  }

  static BluetoothDeviceStateService get deviceState {
    assert(
        null != _deviceStateServiceRef.value,
        "Call BluetoothManager.initialize before "
        "accessing any static members.");

    return _deviceStateServiceRef.value!;
  }

  static BluetoothCommunicationService get communication {
    assert(
        null != _communicationServiceRef.value,
        "Call BluetoothManager.initialize before "
        "accessing any static members.");

    return _communicationServiceRef.value!;
  }

  static Future<void> setNativeLogLevel(Level level) async {
    await _methodChannel.invokeMethod<void>("setNativeLogLevel", {
      "level": _getNativeLogLevel(level),
    });
    _nativeLogLevel = level;
  }

  static Level getNativeLogLevel() => _nativeLogLevel;

  static Future<void> initialize([OnLogCallback? onLogCallback]) async {
    // Return if all members have already been initialized.
    if (null != _stateServiceRef.value &&
        null != _scannerServiceRef.value &&
        null != _deviceStateServiceRef.value &&
        null != _communicationServiceRef.value) {
      _log(message: "The $BluetoothManager has already been initialized.");
      return;
    }

    _onLogCallback = onLogCallback;

    _log(message: "Initializing the $BluetoothManager.");

    _stateServiceRef.value = BluetoothStateService._();
    _scannerServiceRef.value = BluetoothScannerService._();
    _deviceStateServiceRef.value = BluetoothDeviceStateService._();
    _communicationServiceRef.value = BluetoothCommunicationService._(
        _deviceStateServiceRef.constRef<BluetoothDeviceStateService?>());

    await FlutterReactiveBle().initialize();

    _log(message: "Finished initializing the $BluetoothManager.");
  }

  static Future<void> dispose() async {
    _log(message: "Disposing the $BluetoothManager.");

    _communicationServiceRef.value = null;

    await _deviceStateServiceRef.value?.dispose();
    _deviceStateServiceRef.value = null;

    await _scannerServiceRef.value?.dispose();
    _scannerServiceRef.value = null;

    await _stateServiceRef.value?.dispose();
    _stateServiceRef.value = null;

    await FlutterReactiveBle().deinitialize();

    _log(message: "Finished disposing the $BluetoothManager.");
  }
}
