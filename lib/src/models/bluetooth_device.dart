part of "../../bluetooth.dart";

/// A BluetoothDevice that does only compare it's [id], [name], [serviceData],
/// [serviceUuids] and [manufacturerData], because those are values most
/// probably do not change between different scan processes.
///
/// [rssi] can change whenever the location of the advertising device, or the
/// scanning device is changed, which is unwanted behaviour.
@immutable
class BluetoothDevice {
  const BluetoothDevice._(this._device);

  final DiscoveredDevice _device;

  /// The unique identifier of the device.
  String get id => _device.id;

  String get name => _device.name;

  Map<Uuid, Uint8List> get serviceData => _device.serviceData;

  /// Advertised services
  List<Uuid> get serviceUuids => _device.serviceUuids;

  /// Manufacturer specific data.
  ///
  /// The first 2 bytes are the Company Identifier Codes.
  Uint8List get manufacturerData => _device.manufacturerData;

  int get rssi => _device.rssi;

  @override
  int get hashCode {
    var result = 17;

    result = 37 * result + id.hashCode;
    result = 37 * result + name.hashCode;
    result = 37 * result + const DeepCollectionEquality().hash(serviceData);
    result = 37 * result + const DeepCollectionEquality().hash(serviceUuids);
    result =
        37 * result + const DeepCollectionEquality().hash(manufacturerData);

    return result;
  }

  @override
  bool operator ==(Object other) {
    if (other is! BluetoothDevice) return false;

    if (other.id != id) return false;
    if (other.name != name) return false;
    if (!mapEquals(other.serviceData, serviceData)) return false;
    if (!listEquals(other.serviceUuids, serviceUuids)) return false;
    if (!listEquals(other.manufacturerData, manufacturerData)) return false;

    return true;
  }

  @override
  String toString() => "${describeIdentity(this)}"
      "(id: $id, "
      "name: $name, "
      "serviceData: $serviceData, "
      "serviceUuids: $serviceUuids, "
      "manufacturerData: $manufacturerData, "
      "rssi: $rssi)";

  /// If [serviceData], [serviceUuids] or [manufacturerData] is `null`,
  /// then a copy of the representive value of `this` is created.
  BluetoothDevice copyWith({
    String? id,
    String? name,
    Map<Uuid, Uint8List>? serviceData,
    List<Uuid>? serviceUuids,
    Uint8List? manufacturerData,
    int? rssi,
  }) =>
      BluetoothDevice._(
        // Copy collections.
        DiscoveredDevice(
          id: id ?? this.id,
          name: name ?? this.name,
          serviceData: serviceData ?? Map.of(this.serviceData),
          serviceUuids: serviceUuids ?? List.of(this.serviceUuids),
          manufacturerData:
              manufacturerData ?? Uint8List.fromList(this.manufacturerData),
          rssi: rssi ?? this.rssi,
        ),
      );
}
