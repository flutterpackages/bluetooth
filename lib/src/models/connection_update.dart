part of "../../bluetooth.dart";

@immutable
class ConnectionUpdate {
  const ConnectionUpdate._({
    required this.device,
    required this.connectionState,
    this.failure,
  });

  final BluetoothDevice device;

  final DeviceConnectionState connectionState;

  /// Field `error` is null if there is no error reported.
  final GenericFailure<ConnectionError>? failure;

  @override
  int get hashCode {
    var result = 17;
    result = 37 * result + device.hashCode;
    result = 37 * result + connectionState.hashCode;
    result = 37 * result + failure.hashCode;
    return result;
  }

  @override
  bool operator ==(Object other) {
    if (other is! ConnectionUpdate) return false;
    if (other.device != device) return false;
    if (other.connectionState != connectionState) return false;
    if (other.failure != failure) return false;

    return true;
  }

  @override
  String toString() => "${describeIdentity(this)}"
      "(device: $device, "
      "connectionState: $connectionState, "
      "failure: $failure)";

  ConnectionUpdate copyWith({
    BluetoothDevice? device,
    DeviceConnectionState? connectionState,
    GenericFailure<ConnectionError>? failure,
  }) =>
      ConnectionUpdate._(
        device: device ?? this.device,
        connectionState: connectionState ?? this.connectionState,
        failure: failure ?? this.failure,
      );
}
