part of "../bluetooth.dart";

typedef OnFoundDeviceCallback = bool Function(BluetoothDevice device);

typedef OnConnectionStateUpdateCallback = void Function(
    ConnectionStateUpdate update);

typedef OnFutureErrorCallback<T, E> = FutureOr<T> Function(
  E error,
  StackTrace stackTrace,
);

typedef OnExceptionCallback<T extends Exception> = void Function(T exception);

typedef OnLogCallback = void Function(
  Level level,
  Object? message,
  Object? error,
  StackTrace? stackTrace,
);
