part of "../bluetooth.dart";

abstract class _Disposable {
  const _Disposable();

  Future<void> dispose();
}
