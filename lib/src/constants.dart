part of "../bluetooth.dart";

const _namespace = "com.u12ampere.bluetooth";
const _methodChannelId = "$_namespace.methods";
const _stateServiceEventChannelId =
    _namespace + BluetoothStateService._eventChannelId;

const _methodChannel = MethodChannel(_methodChannelId);
const _stateServiceEventChannel = EventChannel(_stateServiceEventChannelId);
