part of "../bluetooth.dart";

void _log({
  Object? message,
  Level level = Level.debug,
  Object? error,
  StackTrace? stackTrace,
}) =>
    BluetoothManager._onLogCallback?.call(
      level,
      message,
      error,
      stackTrace,
    );

int _getNativeLogLevel(Level level) {
  switch (level) {
    case Level.verbose:
      return 2;

    case Level.debug:
      return 3;

    case Level.info:
      return 4;

    case Level.warning:
      return 5;

    case Level.error:
      return 6;

    case Level.wtf:
      return 7;

    case Level.nothing:
      return 8;
  }
}
