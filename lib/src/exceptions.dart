part of "../bluetooth.dart";

/// Base class for all custom exceptions that this package may throw.
class BluetoothException implements Exception {
  const BluetoothException([this.message]);

  final Object? message;

  @override
  String toString() {
    // ignore: no_runtimetype_tostring
    var _message = runtimeType.toString();

    if (null != message) {
      _message = "$_message: $message";
    }

    return _message;
  }
}

/// A [BluetoothException] that gets thrown when the user tries to start
/// a new scan process, even though the previous one has not finished or
/// has not been stopped yet.
class ScanInProgressException extends BluetoothException {
  const ScanInProgressException()
      : super(
          "You cannot start another scan process "
          "while another one is still running.",
        );
}
