part of "../bluetooth.dart";

// DO NOT CHANGE THE ORDER OF THE VALUES!
enum BluetoothState {
  off,
  turningOn,
  on,
  turningOff,

  /// State is not (yet) determined.
  unknown,

  /// Bluetooth is not supported on this device.
  unsupported,

  /// Bluetooth usage is not authorized for this app.
  unauthorized,

  /// Android only: Location services are disabled.
  locationServicesDisabled,
}

/// Convert the native state of the BluetoothAdapter on Android
/// to a [BluetoothState].
///
/// This method is **not compatible** with [flutter_blue](https://github.com/boskokg/flutter_blue.git).
BluetoothState _getBluetoothStatefromNativeState(int? state) {
  if (null == state) return BluetoothState.unknown;
  return BluetoothState.values[state - 10];
}

/// Convert a [BleStatus] to a [BluetoothState].
BluetoothState _getBluetoothStatefromBleStatus(BleStatus? status) {
  switch (status) {
    case BleStatus.unsupported:
      return BluetoothState.unsupported;

    case BleStatus.unauthorized:
      return BluetoothState.unauthorized;

    case BleStatus.poweredOff:
      return BluetoothState.off;

    case BleStatus.locationServicesDisabled:
      return BluetoothState.locationServicesDisabled;

    case BleStatus.ready:
      return BluetoothState.on;

    default:
  }

  return BluetoothState.unknown;
}
