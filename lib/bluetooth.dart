library bluetooth;

import "dart:async";
import "dart:typed_data";

import "package:collection/collection.dart";
import "package:flutter/foundation.dart";
import "package:flutter/services.dart";
import "package:flutter_reactive_ble/flutter_reactive_ble.dart";
import "package:logger/logger.dart" show Level;
import "package:notifiers/notifiers.dart";
import "package:object_reference/object_reference.dart";

export "package:flutter_reactive_ble/flutter_reactive_ble.dart"
    hide
        FlutterReactiveBle,
        DiscoveredDevice,
        //
        $ConnectionStateUpdate,
        $DiscoveredDevice,
        $DiscoveredService,
        ConnectionStateUpdate$,
        ConnectionStateUpdate$Change,
        DiscoveredDevice$,
        DiscoveredDevice$Change,
        DiscoveredService$,
        DiscoveredService$Change;
export "package:logger/logger.dart" show Level;

part "src/bluetooth_manager.dart";
part "src/bluetooth_state.dart";
part "src/constants.dart";
part "src/disposable.dart";
part "src/exceptions.dart";
part "src/models/bluetooth_device.dart";
part "src/models/connection_update.dart";
part "src/services/bluetooth_communication_service.dart";
part "src/services/bluetooth_device_state_service.dart";
part "src/services/bluetooth_scanner_service.dart";
part "src/services/bluetooth_state_service.dart";
part "src/typedefs.dart";
part "src/utils.dart";
