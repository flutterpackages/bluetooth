package com.u12ampere.bluetooth.services;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.u12ampere.bluetooth.Constants;
import com.u12ampere.bluetooth.logger.Logger;
import com.u12ampere.bluetooth.logger.defaults.DefaultLogFilter;
import com.u12ampere.bluetooth.logger.defaults.DefaultLogOutput;
import com.u12ampere.bluetooth.logger.defaults.DefaultLogPrinter;
import com.u12ampere.bluetooth.logger.events.LogEvent;
import com.u12ampere.bluetooth.logger.events.OutputEvent;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

public abstract class BluetoothManager {
    private static boolean m_isInitialized = false;


    @Nullable
    private static Logger m_logger;
    @Logger.LogLevel
    private static int m_logLevel = Logger.VERBOSE;

    @Nullable
    private static BluetoothMethodCallHandler m_callHandler;
    @Nullable
    private static MethodChannel m_methodChannel;

    @Nullable
    private static EventChannel m_stateServiceEventChannel;


    @Nullable
    private static BluetoothStateService m_state;


    public static final String LOG_TAG = BluetoothManager.class.getSimpleName();

    /**
     * Throws an {@link IllegalStateException} if
     * {@link BluetoothManager#initialize(FlutterPlugin.FlutterPluginBinding)} has not been
     * called yet.
     */
    @NonNull
    static Logger getLogger() throws IllegalStateException {
        if (null == m_logger)
            throw new IllegalStateException("Call BluetoothManager.initialize before "
                    + "accessing any static properties.");

        return m_logger;
    }

    public static boolean isInitialized() {
        return m_isInitialized;
    }

    /**
     * Throws an {@link IllegalStateException} if
     * {@link BluetoothManager#initialize(FlutterPlugin.FlutterPluginBinding)} has not been
     * called yet.
     */
    @NonNull
    public static BluetoothStateService getStateService() {
        if (null == m_state)
            throw new IllegalStateException("Call BluetoothManager.initialize before "
                    + "accessing any static properties.");

        return m_state;
    }

    public static void initialize(FlutterPlugin.FlutterPluginBinding binding) {
        if (m_isInitialized) return;

        m_logger = new Logger(
                new DefaultLogPrinter(),
                new DefaultLogOutput() {
                    @Override
                    public void output(@NonNull OutputEvent event) {
                        for (int i = 0; i < event.lines.length; i++) {
                            Log.println(event.level, LOG_TAG, event.lines[i]);
                        }
                    }
                },
                new DefaultLogFilter() {
                    @Override
                    public boolean shouldLog(@NonNull LogEvent event) {
                        return event.level >= m_logLevel;
                    }
                }
        );


        m_callHandler = new BluetoothMethodCallHandler();
        m_methodChannel = new MethodChannel(
                binding.getBinaryMessenger(),
                Constants.METHODCHANNEL_ID
        );
        m_methodChannel.setMethodCallHandler(m_callHandler);

        m_stateServiceEventChannel = new EventChannel(
                binding.getBinaryMessenger(),
                Constants.STATE_SERVICE_EVENTCHANNEL_ID
        );


        m_state = new BluetoothStateService(
                m_stateServiceEventChannel,
                binding.getApplicationContext()
        );


        m_isInitialized = true;
        getLogger().d("Successfully initialized BluetoothManager.");
    }

    public static void dispose() {
        m_callHandler = null;
        //noinspection ConstantConditions
        m_methodChannel.setMethodCallHandler(null);
        m_methodChannel = null;

        m_stateServiceEventChannel = null;

        getStateService().dispose();
        m_state = null;

        m_isInitialized = false;

        // m_logger has to be the last object to be disposed of, because
        // some classes may use it in their dispose routine.
        getLogger().d("Disposed BluetoothManager.");
        getLogger().dispose();

        m_logger = null;
    }

    private static class BluetoothMethodCallHandler implements MethodChannel.MethodCallHandler {
        @Override
        public void onMethodCall(@NonNull MethodCall call, @NonNull MethodChannel.Result result) {
            if (call.method.equals("setNativeLogLevel")) {
                BluetoothManager.m_logLevel = call.argument("level");
                result.success(null);
                return;
            }

            if (call.method.equals("enable")) {
                result.success(BluetoothManager.getStateService().enable());
                return;
            }

            if (call.method.equals("disable")) {
                result.success(BluetoothManager.getStateService().disable());
                return;
            }

            if (call.method.equals("getState")) {
                result.success(BluetoothManager.getStateService().getState());
                return;
            }

            result.notImplemented();
        }
    }
}
