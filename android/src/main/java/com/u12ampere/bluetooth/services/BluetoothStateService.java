package com.u12ampere.bluetooth.services;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import androidx.annotation.NonNull;

import com.u12ampere.bluetooth.interfaces.IDisposable;

import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.MethodChannel;

public class BluetoothStateService implements IDisposable {
    BluetoothStateService(@NonNull EventChannel eventChannel, @NonNull Context applicationContext) {
        m_applicationContext = applicationContext;
        m_eventChannel = eventChannel;

        m_bluetoothStateChangedReceiver = new BluetoothStateChangedReceiver();
        m_eventChannel.setStreamHandler(m_bluetoothStateChangedReceiver);

        m_applicationContext.registerReceiver(
                m_bluetoothStateChangedReceiver,
                m_bluetoothStateChangedReceiver.getIntentFilter()
        );

        m_androidBTManager = (android.bluetooth.BluetoothManager)
                m_applicationContext.getSystemService(Context.BLUETOOTH_SERVICE);

        BluetoothManager.getLogger().d("Successfully constructed BluetoothStateService.");
    }

    public static final String EVENTCHANNEL_ID = ".state_service";

    private EventChannel m_eventChannel;
    private BluetoothStateChangedReceiver m_bluetoothStateChangedReceiver;
    private Context m_applicationContext;

    private android.bluetooth.BluetoothManager m_androidBTManager;

    /**
     * Get the current state of the local Bluetooth adapter.
     * <br>
     * <br>
     * Possible return values are {@link BluetoothAdapter#STATE_OFF},
     * {@link BluetoothAdapter#STATE_TURNING_ON}, {@link BluetoothAdapter#STATE_ON},
     * {@link BluetoothAdapter#STATE_TURNING_OFF}.
     */
    public int getState() {
        return m_androidBTManager.getAdapter().getState();
    }

    /**
     * This is an asynchronous call: it will return immediately.
     * <br>
     * <br>
     * If this call returns true, then the adapter state will immediately transition from
     * {@link BluetoothAdapter#STATE_OFF} to {@link BluetoothAdapter#STATE_TURNING_ON}, and
     * some time later transition to either {@link BluetoothAdapter#STATE_OFF} or
     * {@link BluetoothAdapter#STATE_ON}.
     * <br>
     * <br>
     * If this call returns false then there was an immediate problem that will prevent the
     * adapter from being turned on - such as Airplane mode, or the adapter is already
     * turned on.
     */
    public boolean enable() {
        return m_androidBTManager.getAdapter().enable();
    }

    /**
     * This is an asynchronous call: it will return immediately.
     * <br>
     * <br>
     * If this call returns true, then the state will immediately transition from
     * {@link BluetoothAdapter#STATE_ON} to {@link BluetoothAdapter#STATE_TURNING_OFF},
     * and some time later transition to either {@link BluetoothAdapter#STATE_OFF} or
     * {@link BluetoothAdapter#STATE_ON}.
     * <br>
     * <br>
     * If this call returns false then there was an immediate problem that will prevent the
     * adapter from being turned off - such as the adapter already being turned off.
     */
    public boolean disable() {
        return m_androidBTManager.getAdapter().disable();
    }

    @Override
    public void dispose() {
        m_applicationContext.unregisterReceiver(m_bluetoothStateChangedReceiver);
        m_applicationContext = null;

        m_eventChannel.setStreamHandler(null);
        m_eventChannel = null;

        m_bluetoothStateChangedReceiver = null;

        m_androidBTManager = null;

        BluetoothManager.getLogger().d("Disposed BluetoothStateService.");
    }

    // BluetoothStateChangedReceiver is only needed in BluetoothService,
    // therefore make it private and static.
    private static class BluetoothStateChangedReceiver extends BroadcastReceiver
            implements EventChannel.StreamHandler {
        BluetoothStateChangedReceiver() {
        }

        private EventChannel.EventSink sink;

        IntentFilter getIntentFilter() {
            return new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (!BluetoothAdapter.ACTION_STATE_CHANGED.equals(intent.getAction())) return;

            final int state = intent.getIntExtra(
                    BluetoothAdapter.EXTRA_STATE,
                    BluetoothAdapter.STATE_TURNING_OFF + 1
            );

            if (null == sink) {
                BluetoothManager.getLogger().w("Received state '" + state + "', but the EventSink is null.");
                return;
            }

            sink.success(state);
        }

        @Override
        public void onListen(Object arguments, EventChannel.EventSink events) {
            sink = events;
        }

        @Override
        public void onCancel(Object arguments) {
            sink = null;
        }
    }
}
