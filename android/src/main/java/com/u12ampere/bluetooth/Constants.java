package com.u12ampere.bluetooth;

import com.u12ampere.bluetooth.services.BluetoothStateService;

public final class Constants {
    public static final String NAMESPACE = "com.u12ampere.bluetooth";

    public static final String METHODCHANNEL_ID = NAMESPACE + ".methods";

    public static final String STATE_SERVICE_EVENTCHANNEL_ID =
            NAMESPACE + BluetoothStateService.EVENTCHANNEL_ID;

}
