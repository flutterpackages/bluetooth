package com.u12ampere.bluetooth;

import androidx.annotation.NonNull;

import com.u12ampere.bluetooth.services.BluetoothManager;

import io.flutter.embedding.engine.plugins.FlutterPlugin;

public class BluetoothPlugin implements FlutterPlugin {
    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding binding) {
        BluetoothManager.initialize(binding);
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        BluetoothManager.dispose();
    }
}
