package com.u12ampere.bluetooth.logger.handlers;

import androidx.annotation.NonNull;

import com.u12ampere.bluetooth.interfaces.ILifeCycle;
import com.u12ampere.bluetooth.logger.events.OutputEvent;

/**
 * Log output receives a {@link OutputEvent} from {@link ILogPrinter} and sends it
 * to the desired destination.
 * <br>
 * <br>
 * This can be an output stream, a file or a network target. {@link ILogOutput} may
 * cache multiple log messages.
 */
public interface ILogOutput extends ILifeCycle {
    void output(@NonNull OutputEvent event);
}
