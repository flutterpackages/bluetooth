package com.u12ampere.bluetooth.logger.events;

import androidx.annotation.Nullable;

import com.u12ampere.bluetooth.logger.Logger;

public class LogEvent {
    public LogEvent(
            @Logger.LogLevel int level, @Nullable Object message,
            @Nullable Object error, @Nullable StackTraceElement[] stackTrace) {
        this.level = level;
        this.message = message;
        this.error = error;
        this.stackTrace = stackTrace;
    }

    @Logger.LogLevel
    public final int level;

    @Nullable
    public final Object message;

    @Nullable
    public final Object error;

    @Nullable
    public final StackTraceElement[] stackTrace;
}
