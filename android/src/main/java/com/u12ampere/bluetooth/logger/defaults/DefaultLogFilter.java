package com.u12ampere.bluetooth.logger.defaults;

import androidx.annotation.NonNull;

import com.u12ampere.bluetooth.logger.events.LogEvent;
import com.u12ampere.bluetooth.logger.handlers.ILogFilter;

public class DefaultLogFilter implements ILogFilter {
    @Override
    public void dispose() {

    }

    @Override
    public void initialize() {

    }

    @Override
    public boolean shouldLog(@NonNull LogEvent event) {
        return true;
    }
}
