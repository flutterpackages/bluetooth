package com.u12ampere.bluetooth.logger;

import static java.lang.annotation.RetentionPolicy.SOURCE;

import android.util.Log;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.u12ampere.bluetooth.interfaces.IDisposable;
import com.u12ampere.bluetooth.logger.defaults.DefaultLogFilter;
import com.u12ampere.bluetooth.logger.defaults.DefaultLogOutput;
import com.u12ampere.bluetooth.logger.defaults.DefaultLogPrinter;
import com.u12ampere.bluetooth.logger.events.LogEvent;
import com.u12ampere.bluetooth.logger.events.OutputEvent;
import com.u12ampere.bluetooth.logger.handlers.ILogFilter;
import com.u12ampere.bluetooth.logger.handlers.ILogOutput;
import com.u12ampere.bluetooth.logger.handlers.ILogPrinter;

import java.lang.annotation.Retention;

public class Logger implements IDisposable {
    public Logger(
            @NonNull ILogPrinter printer, @NonNull ILogOutput output,
            @NonNull ILogFilter filter) {
        m_printer = printer;
        m_output = output;
        m_filter = filter;

        m_printer.initialize();
        m_output.initialize();
        m_filter.initialize();

        m_isActive = true;
    }

    /**
     * Instantiates a new {@link Logger} with {@link DefaultLogPrinter}, {@link DefaultLogOutput}
     * and {@link DefaultLogFilter}.
     */
    public Logger() {
        this(new DefaultLogPrinter(), new DefaultLogOutput(), new DefaultLogFilter());
    }

    @Nullable
    private ILogPrinter m_printer;

    @Nullable
    private ILogOutput m_output;

    @Nullable
    private ILogFilter m_filter;

    private boolean m_isActive;

    /**
     * Once {@link Logger#dispose()} has been called, this method
     * always returns false.
     * <br>
     * <br>
     * You can change the state of this {@link Logger} by calling {@link Logger#activate()}
     * or {@link Logger#deactivate()}.
     *
     * @return Whether this {@link Logger} is active or not.
     */
    public boolean isActive() {
        if (null == m_printer) return false;
        if (null == m_output) return false;
        if (null == m_filter) return false;

        return m_isActive;
    }

    public void activate() {
        m_isActive = true;
    }

    public void deactivate() {
        m_isActive = false;
    }

    public void v(@Nullable Object message,
                  @Nullable Object error, @Nullable StackTraceElement[] stackTrace
    ) throws IllegalArgumentException {
        log(VERBOSE, message, error, stackTrace);
    }

    public void v(@Nullable Object message) {
        // Cannot throw IllegalArgumentException, because error is null.
        v(message, null, null);
    }

    public void d(@Nullable Object message,
                  @Nullable Object error, @Nullable StackTraceElement[] stackTrace
    ) throws IllegalArgumentException {
        log(DEBUG, message, error, stackTrace);
    }

    public void d(@Nullable Object message) {
        // Cannot throw IllegalArgumentException, because error is null.
        d(message, null, null);
    }

    public void i(@Nullable Object message,
                  @Nullable Object error, @Nullable StackTraceElement[] stackTrace
    ) throws IllegalArgumentException {
        log(INFO, message, error, stackTrace);
    }

    public void i(@Nullable Object message) {
        // Cannot throw IllegalArgumentException, because error is null.
        i(message, null, null);
    }

    public void w(@Nullable Object message,
                  @Nullable Object error, @Nullable StackTraceElement[] stackTrace
    ) throws IllegalArgumentException {
        log(WARNING, message, error, stackTrace);
    }

    public void w(@Nullable Object message) {
        // Cannot throw IllegalArgumentException, because error is null.
        w(message, null, null);
    }

    public void e(@Nullable Object message,
                  @Nullable Object error, @Nullable StackTraceElement[] stackTrace
    ) throws IllegalArgumentException {
        log(ERROR, message, error, stackTrace);
    }

    public void e(@Nullable Object message) {
        // Cannot throw IllegalArgumentException, because error is null.
        e(message, null, null);
    }

    public void wtf(@Nullable Object message,
                    @Nullable Object error, @Nullable StackTraceElement[] stackTrace
    ) throws IllegalArgumentException {
        log(WTF, message, error, stackTrace);
    }

    public void wtf(@Nullable Object message) {
        // Cannot throw IllegalArgumentException, because error is null.
        wtf(message, null, null);
    }

    /**
     * No {@link LogEvent}s will be emitted if {@link Logger#isActive()} returns false.
     */
    public void log(@LogLevel int level, @Nullable Object message) {
        // Cannot throw IllegalArgumentException, because error is null.
        log(level, message, null, null);
    }

    /**
     * Throws an {@link IllegalArgumentException} if error is an instance of {@link StackTraceElement}
     * of an array of {@link StackTraceElement}s.
     * <br>
     * <br>
     * No {@link LogEvent}s will be emitted if {@link Logger#isActive()} returns false.
     */
    public void log(
            @LogLevel int level, @Nullable Object message,
            @Nullable Object error, @Nullable StackTraceElement[] stackTrace
    ) throws IllegalArgumentException {
        if (!isActive()) return;

        assert (NOTHING > level);

        if (error instanceof StackTraceElement || error instanceof StackTraceElement[])
            throw new IllegalArgumentException(
                    "Error parameter cannot be a StackTraceElement or "
                            + "an array of StackTraceElements!");

        final LogEvent event = new LogEvent(level, message, error, stackTrace);

        //noinspection ConstantConditions
        if (!m_filter.shouldLog(event))
            return;

        //noinspection ConstantConditions
        final String[] lines = m_printer.log(event);

        final OutputEvent outputEvent = new OutputEvent(level, lines);

        //noinspection ConstantConditions
        m_output.output(outputEvent);
    }

    @Override
    public void dispose() {
        // If not active then this logger had already been disposed.
        if (!isActive()) return;

        m_isActive = false;

        // Suppress NullPointerExceptions, because all log handlers
        // are only set to null in dispose.

        //noinspection ConstantConditions
        m_printer.dispose();
        m_printer = null;

        //noinspection ConstantConditions
        m_output.dispose();
        m_output = null;

        //noinspection ConstantConditions
        m_filter.dispose();
        m_filter = null;
    }


    @Retention(SOURCE)
    @IntDef({VERBOSE, DEBUG, INFO, WARNING, ERROR, WTF, NOTHING})
    public @interface LogLevel {
    }

    public static final int VERBOSE = Log.VERBOSE;
    public static final int DEBUG = Log.DEBUG;
    public static final int INFO = Log.INFO;
    public static final int WARNING = Log.WARN;
    public static final int ERROR = Log.ERROR;
    public static final int WTF = Log.ERROR + 1;
    public static final int NOTHING = WTF + 1;
}
