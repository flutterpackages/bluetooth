package com.u12ampere.bluetooth.logger.defaults;

import androidx.annotation.NonNull;

import com.u12ampere.bluetooth.logger.events.LogEvent;
import com.u12ampere.bluetooth.logger.handlers.ILogPrinter;

public class DefaultLogPrinter implements ILogPrinter {
    @Override
    public void dispose() {

    }

    @Override
    public void initialize() {

    }

    @NonNull
    @Override
    public String[] log(@NonNull LogEvent event) {
        final String[] lines = new String[4];

        lines[0] = "Level: " + event.level;
        lines[1] = "Message: " + event.message;
        lines[2] = "Error: " + event.error;

        if (null == event.stackTrace) {
            lines[3] = "Stacktrace: " + null;
            return lines;
        }

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < event.stackTrace.length; i++) {
            builder.append(event.stackTrace[i]).append('\n');
        }
        lines[3] = builder.toString();

        return lines;
    }
}
