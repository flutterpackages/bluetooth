package com.u12ampere.bluetooth.logger.handlers;

import androidx.annotation.NonNull;

import com.u12ampere.bluetooth.interfaces.ILifeCycle;
import com.u12ampere.bluetooth.logger.events.LogEvent;

/**
 * A filter for {@link LogEvent}s.
 */
public interface ILogFilter extends ILifeCycle {

    /**
     * Is called every time a new {@link LogEvent} is sent and decides if
     * it will be printed or canceled.
     * <br>
     * <br>
     *
     * @return True if the event should be logged, else false.
     */
    boolean shouldLog(@NonNull LogEvent event);
}
