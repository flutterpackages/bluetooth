package com.u12ampere.bluetooth.logger.handlers;

import androidx.annotation.NonNull;

import com.u12ampere.bluetooth.interfaces.ILifeCycle;
import com.u12ampere.bluetooth.logger.Logger;
import com.u12ampere.bluetooth.logger.events.LogEvent;

/**
 * A handler of log events.
 * <br>
 * <br>
 * A log printer creates and formats the output, which is then sent to
 * {@link ILogOutput} of a {@link Logger}.
 */
public interface ILogPrinter extends ILifeCycle {

    /**
     * This method is called every time a new {@link LogEvent} is sent. It handles the
     * formatting of  the message.
     */
    @NonNull
    String[] log(@NonNull LogEvent event);
}
