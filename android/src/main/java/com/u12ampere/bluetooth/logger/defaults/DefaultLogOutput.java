package com.u12ampere.bluetooth.logger.defaults;

import android.util.Log;

import androidx.annotation.NonNull;

import com.u12ampere.bluetooth.logger.events.OutputEvent;
import com.u12ampere.bluetooth.logger.handlers.ILogOutput;

public class DefaultLogOutput implements ILogOutput {
    @Override
    public void dispose() {
    }

    @Override
    public void initialize() {
    }

    @Override
    public void output(@NonNull OutputEvent event) {
        for (int i = 0; i < event.lines.length; i++) {
            Log.println(event.level, "DefaultLogOutput", event.lines[i]);
        }
    }
}
