package com.u12ampere.bluetooth.logger.events;

import androidx.annotation.NonNull;

import com.u12ampere.bluetooth.logger.Logger;

public class OutputEvent {
    public OutputEvent(@Logger.LogLevel int level, @NonNull String[] lines) {
        this.level = level;
        this.lines = lines;
    }

    @Logger.LogLevel
    public final int level;

    @NonNull
    public final String[] lines;
}
