package com.u12ampere.bluetooth.interfaces;

public interface ILifeCycle extends IDisposable {

    /**
     * This method handles initialization processes that could not be done
     * in the constructor.
     */
    void initialize();
}
