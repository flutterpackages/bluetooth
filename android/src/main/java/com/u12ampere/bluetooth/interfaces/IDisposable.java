package com.u12ampere.bluetooth.interfaces;

public interface IDisposable {
    /**
     * Dispose the object this method was called on.
     * <br>
     * <br>
     * This method should only be called once.
     */
    void dispose();
}
