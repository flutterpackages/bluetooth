import "package:flutter/foundation.dart";
import "package:flutter_test/flutter_test.dart";

void main() {
  test("Test with addEntries", () {
    final map = {_Key("nona"): "first"};
    expect(map, {_Key("nona"): "first"});

    final entry2 = MapEntry(_Key("joch"), "nix");
    map.addEntries([entry2]);
    expect(map, {
      _Key("nona"): "first",
      _Key("joch"): "nix",
    });
  });
}

class _Key {
  _Key([this.s = "Default"]);

  String s;

  @override
  bool operator ==(Object other) {
    if (other is! _Key) return false;
    if (other.s != s) return false;
    return true;
  }

  @override
  int get hashCode => s.hashCode;

  @override
  String toString() => "${describeIdentity(this)}($s)";
}
