#import "BluetoothPlugin.h"
#if __has_include(<bluetooth/bluetooth-Swift.h>)
#import <bluetooth/bluetooth-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "bluetooth-Swift.h"
#endif

@implementation BluetoothPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftBluetoothPlugin registerWithRegistrar:registrar];
}
@end
