import Flutter
import UIKit

public class SwiftBluetoothPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "bluetooth", binaryMessenger: registrar.messenger())
    let instance = SwiftBluetoothPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    if call.method == "enable" { 
      result(nil)
      return
    }

    if call.method == "disable" { 
      result(nil)
      return
    }
    
    if call.method == "getState" { 
      result(nil)
      return
    }

    result(FlutterMethodNotImplemented)
  }
}