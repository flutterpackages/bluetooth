import "dart:math";

import "package:flutter/material.dart";

/*------------------------------------------------------------------*/
/*                            Methods                               */
/*------------------------------------------------------------------*/

/// percent has to be within [0;1]
double getWindowWidth(BuildContext context, [double percent = 1]) {
  assert(percent >= 0 || percent <= 1,
      "\ngetWindowWidth: percent has to be within [0;1]\n");

  return MediaQuery.of(context).size.width * percent;
}

/// percent has to be within [0;1]
double getWindowHeight(BuildContext context, [double percent = 1]) {
  assert(percent >= 0 || percent <= 1,
      "\ngetWindowWidth: percent has to be within [0;1]\n");

  return MediaQuery.of(context).size.height * percent;
}

/*------------------------------------------------------------------*/
/*                            Extensions                            */
/*------------------------------------------------------------------*/

extension RandomElement<T> on Iterable<T> {
  T getRandom([Random? random]) {
    random ??= Random();
    return elementAt(random.nextInt(length));
  }
}

extension Operators on StringBuffer {
  /// Abbrevation for [write]
  StringBuffer operator +(Object? object) {
    write(object);
    return this;
  }
}
