import "package:bluetooth/bluetooth.dart";
import "package:flutter/material.dart";
import "package:flutter/services.dart";
import "package:request_permission/request_permission.dart";
import "package:wakelock/wakelock.dart";

import "home.dart";

class Application extends StatelessWidget {
  const Application._();

  static Future<void> run() async {
    //Tries to only allow Portrait mode, if an Error occures
    //it launches anyway but with Portrait and landscape
    await SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    RequestPermission.instace.results.listen((event) {
      event.grantedPermissions.forEach((key, value) => print("$key: $value"));
    });

    await RequestPermission.instace.requestMultipleAndroidPermissions({
      AndroidPermissions.bluetooth,
      AndroidPermissions.bluetoothAdmin,
      AndroidPermissions.accessFineLocation,
    });

    await Wakelock.enable();

    await BluetoothManager.initialize();

    runApp(const Application._());
  }

  @override
  Widget build(BuildContext context) => MaterialApp(
        title: "Flutter Demo",
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const Home(),
      );
}
