import "package:bluetooth/bluetooth.dart";
import "package:flutter/material.dart";

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) => Scaffold(
        body: SafeArea(
          child: SizedBox(
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: () {},
                  child: const Text("Enable"),
                ),
                ElevatedButton(
                  onPressed: BluetoothManager.state.disable,
                  child: const Text("Disable"),
                ),
              ],
            ),
          ),
        ),
      );
}
