## 2.2.1

  * Minor changes to the implementation of `BluetoothScannerService`. The timeout parameter for `scan` now works as expected.

## 2.2.0

  * **[pubspec.yaml](pubspec.yaml)**:

    * Uncommented pluginClass for IOS


  * **[bluetooth.podspec](ios/bluetooth.podspec)**
    
    * Changed min IOS version from `8.0` to `12.0`.

  * **[SwiftReactiveBlePlugin.swift](/home/emanuel/.pub-cache/hosted/pub.dartlang.org/reactive_ble_mobile-5.0.2/ios/Classes/Plugin/SwiftReactiveBlePlugin.swift)**

    * Added handlers for native method calls. Currently all handlers just return `nil`, so that the Plugin is usable on IOS without crashing when a native method call gets invoked.

## 2.1.2

  * **[pubspec.yaml](pubspec.yaml)**:
    
    * Updated **[notifiers](https://gitlab.com/flutterpackages/notifiers.git)** from `v1.0.1` to `v1.0.6`.

  * Added more details to **[LICENSE](LICENSE)**.

## 2.1.1

  * Added **[connection_update.dart](lib/src/models/connection_update.dart)** which contains the class `ConnectionUpdate`.

  * **[bluetooth_device_state_service.dart](lib/src/services/bluetooth_device_state_service.dart)**:
    
    * `BluetoothDeviceStateService`
      
      * Added getter `connectionUpdates`, for detailed information read the dartdoc.

## 2.1.0

  * Removed all dependencies of [flutter_blue](https://github.com/boskokg/flutter_blue.git).

## 2.0.2

  * **[bluetooth_device_state_service.dart](lib/src/services/bluetooth_device_state_service.dart)**:
    
    * `BluetoothDeviceStateService`
      
      * Renamed getter `deviceStates` to `states`. 

---

  * Added **[bluetooth_device.dart](lib/src/models/bluetooth_device.dart)**, which contains the immutable model `BluetoothDevice`, that replaces all occurences of  `DiscoveredDevice` from [flutter_reactive_ble](https://pub.dev/packages/flutter_reactive_ble).

## 2.0.1

* **[bluetooth_manager.dart](lib/src/bluetooth_manager.dart)**:
  
  * Renamed getter `stateService` to `communication`. 

  * Renamed getter `scannerService` to `communication`. 

  * Renamed getter `deviceStateService` to `communication`. 

  * Renamed getter `communicationService` to `communication`. 

---

  * **[bluetooth_scanner_service.dart](lib/src/bluetooth_services/bluetooth_scanner_service.dart)**:
    
    * Added class `_ScanCompleter`.

    * `BluetoothScannerService`
      
      * Added method `stopScan`.

---

  * **[bluetooth_device_state_service.dart](lib/src/bluetooth_services/bluetooth_device_state_service.dart)**:
    
    * `BluetoothDeviceStateService`
    
      * Added getter `deviceStates`. 

## 2.0.0

  * Upgraded Android gradle version from `4.1.0` to `7.0.2`.
   
  * Upgraded Android `compileSdkVersion` from `30` to `31`. 
   
  * Upgraded Android `targetSdkVersion` from `30` to `31`. 

  * Added an interface for basic bluetooth functionality for Android and IOS from [flutter_reactive_ble](https://pub.dev/packages/flutter_reactive_ble).

## 1.0.0

  * Added functionality for enabling and disabling Bluetooth on Android.
  
  * Added [LICENSE](LICENSE)
  
  * Added [README.md](README.md)